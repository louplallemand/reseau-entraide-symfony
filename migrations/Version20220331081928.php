<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220331081928 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE vote (id INT AUTO_INCREMENT NOT NULL, post_relation_id INT NOT NULL, user_relation_id INT NOT NULL, comment_relation_id INT NOT NULL, INDEX IDX_5A108564DE161B74 (post_relation_id), INDEX IDX_5A1085649B4D58CE (user_relation_id), INDEX IDX_5A1085649BA5019 (comment_relation_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE vote ADD CONSTRAINT FK_5A108564DE161B74 FOREIGN KEY (post_relation_id) REFERENCES posts (id)');
        $this->addSql('ALTER TABLE vote ADD CONSTRAINT FK_5A1085649B4D58CE FOREIGN KEY (user_relation_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE vote ADD CONSTRAINT FK_5A1085649BA5019 FOREIGN KEY (comment_relation_id) REFERENCES comments (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE vote');
    }
}
