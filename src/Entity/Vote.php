<?php

namespace App\Entity;

use App\Repository\VoteRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: VoteRepository::class)]
class Vote
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\ManyToOne(targetEntity: Posts::class, inversedBy: 'Votes')]
    #[ORM\JoinColumn(nullable: false)]
    private $PostRelation;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'Votes')]
    #[ORM\JoinColumn(nullable: false)]
    private $UserRelation;

    #[ORM\ManyToOne(targetEntity: Comments::class, inversedBy: 'Votes')]
    #[ORM\JoinColumn(nullable: false)]
    private $CommentRelation;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPostRelation(): ?Posts
    {
        return $this->PostRelation;
    }

    public function setPostRelation(?Posts $PostRelation): self
    {
        $this->PostRelation = $PostRelation;

        return $this;
    }

    public function getUserRelation(): ?User
    {
        return $this->UserRelation;
    }

    public function setUserRelation(?User $UserRelation): self
    {
        $this->UserRelation = $UserRelation;

        return $this;
    }

    public function getCommentRelation(): ?Comments
    {
        return $this->CommentRelation;
    }

    public function setCommentRelation(?Comments $CommentRelation): self
    {
        $this->CommentRelation = $CommentRelation;

        return $this;
    }
}
