<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[UniqueEntity(fields: ['username'], message: 'There is already an account with this username')]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 180, unique: true)]
    private $username;

    #[ORM\Column(type: 'json')]
    private $roles = [];

    #[ORM\Column(type: 'string')]
    private $password;

    #[ORM\OneToMany(mappedBy: 'userRel', targetEntity: Posts::class)]
    private $userPosts;

    #[ORM\OneToMany(mappedBy: 'relUser', targetEntity: Comments::class)]
    private $userComments;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $avatar;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $firstname;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $lastname;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $email;

    #[ORM\OneToMany(mappedBy: 'UserRelation', targetEntity: Vote::class)]
    private $Votes;

    public function __construct()
    {
        $this->userPosts = new ArrayCollection();
        $this->userComments = new ArrayCollection();
        $this->Votes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->username;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return Collection<int, Posts>
     */
    public function getUserPosts(): Collection
    {
        return $this->userPosts;
    }

    public function addUserPost(Posts $userPost): self
    {
        if (!$this->userPosts->contains($userPost)) {
            $this->userPosts[] = $userPost;
            $userPost->setUserRel($this);
        }

        return $this;
    }

    public function removeUserPost(Posts $userPost): self
    {
        if ($this->userPosts->removeElement($userPost)) {
            // set the owning side to null (unless already changed)
            if ($userPost->getUserRel() === $this) {
                $userPost->setUserRel(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Comments>
     */
    public function getUserComments(): Collection
    {
        return $this->userComments;
    }

    public function addUserComment(Comments $userComment): self
    {
        if (!$this->userComments->contains($userComment)) {
            $this->userComments[] = $userComment;
            $userComment->setRelUser($this);
        }

        return $this;
    }

    public function removeUserComment(Comments $userComment): self
    {
        if ($this->userComments->removeElement($userComment)) {
            // set the owning side to null (unless already changed)
            if ($userComment->getRelUser() === $this) {
                $userComment->setRelUser(null);
            }
        }

        return $this;
    }

    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    public function setAvatar(?string $avatar): self
    {
        $this->avatar = $avatar;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(?string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(?string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return Collection<int, Vote>
     */
    public function getVotes(): Collection
    {
        return $this->Votes;
    }

    public function addVote(Vote $vote): self
    {
        if (!$this->Votes->contains($vote)) {
            $this->Votes[] = $vote;
            $vote->setUserRelation($this);
        }

        return $this;
    }

    public function removeVote(Vote $vote): self
    {
        if ($this->Votes->removeElement($vote)) {
            // set the owning side to null (unless already changed)
            if ($vote->getUserRelation() === $this) {
                $vote->setUserRelation(null);
            }
        }

        return $this;
    }
}
