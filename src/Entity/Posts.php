<?php

namespace App\Entity;

use App\Repository\PostsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PostsRepository::class)]
class Posts
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'text')]
    private $question;

    #[ORM\Column(type: 'string', length: 255)]
    private $state;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'userPosts')]
    #[ORM\JoinColumn(nullable: false)]
    private $userRel;

    #[ORM\OneToMany(mappedBy: 'relPosts', targetEntity: Comments::class)]
    private $postComments;

    #[ORM\OneToMany(mappedBy: 'PostRelation', targetEntity: Vote::class)]
    private $Votes;

    public function __construct()
    {
        $this->postComments = new ArrayCollection();
        $this->Votes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuestion(): ?string
    {
        return $this->question;
    }

    public function setQuestion(string $question): self
    {
        $this->question = $question;

        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(string $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getUserRel(): ?User
    {
        return $this->userRel;
    }

    public function setUserRel(?User $userRel): self
    {
        $this->userRel = $userRel;

        return $this;
    }

    /**
     * @return Collection<int, Comments>
     */
    public function getPostComments(): Collection
    {
        return $this->postComments;
    }

    public function addPostComment(Comments $postComment): self
    {
        if (!$this->postComments->contains($postComment)) {
            $this->postComments[] = $postComment;
            $postComment->setRelPosts($this);
        }

        return $this;
    }

    public function removePostComment(Comments $postComment): self
    {
        if ($this->postComments->removeElement($postComment)) {
            // set the owning side to null (unless already changed)
            if ($postComment->getRelPosts() === $this) {
                $postComment->setRelPosts(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Vote>
     */
    public function getVotes(): Collection
    {
        return $this->Votes;
    }

    public function addVote(Vote $vote): self
    {
        if (!$this->Votes->contains($vote)) {
            $this->Votes[] = $vote;
            $vote->setPostRelation($this);
        }

        return $this;
    }

    public function removeVote(Vote $vote): self
    {
        if ($this->Votes->removeElement($vote)) {
            // set the owning side to null (unless already changed)
            if ($vote->getPostRelation() === $this) {
                $vote->setPostRelation(null);
            }
        }

        return $this;
    }
}
