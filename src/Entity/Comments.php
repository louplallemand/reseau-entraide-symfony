<?php

namespace App\Entity;

use App\Repository\CommentsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CommentsRepository::class)]
class Comments
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'text')]
    private $content;

    #[ORM\Column(type: 'integer')]
    private $reputation;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'userComments')]
    #[ORM\JoinColumn(nullable: false)]
    private $relUser;

    #[ORM\ManyToOne(targetEntity: Posts::class, inversedBy: 'postComments')]
    #[ORM\JoinColumn(nullable: false)]
    private $relPosts;

    #[ORM\OneToMany(mappedBy: 'CommentRelation', targetEntity: Vote::class)]
    private $Votes;

    public function __construct()
    {
        $this->Votes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getReputation(): ?int
    {
        return $this->reputation;
    }

    public function setReputation(int $reputation): self
    {
        $this->reputation = $reputation;

        return $this;
    }

    public function getRelUser(): ?User
    {
        return $this->relUser;
    }

    public function setRelUser(?User $relUser): self
    {
        $this->relUser = $relUser;

        return $this;
    }

    public function getRelPosts(): ?Posts
    {
        return $this->relPosts;
    }

    public function setRelPosts(?Posts $relPosts): self
    {
        $this->relPosts = $relPosts;

        return $this;
    }

    /**
     * @return Collection<int, Vote>
     */
    public function getVotes(): Collection
    {
        return $this->Votes;
    }

    public function addVote(Vote $vote): self
    {
        if (!$this->Votes->contains($vote)) {
            $this->Votes[] = $vote;
            $vote->setCommentRelation($this);
        }

        return $this;
    }

    public function removeVote(Vote $vote): self
    {
        if ($this->Votes->removeElement($vote)) {
            // set the owning side to null (unless already changed)
            if ($vote->getCommentRelation() === $this) {
                $vote->setCommentRelation(null);
            }
        }

        return $this;
    }
}
