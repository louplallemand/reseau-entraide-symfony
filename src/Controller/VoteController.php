<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Posts;
use App\Entity\Comments;
use App\Entity\User;
use App\Entity\Vote;
use App\Form\PostFormType;
use App\Form\CommentFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use App\Form\RegisterNewProjectType;

class VoteController extends AbstractController
{
    #[Route('/post/{id}/vote/{ID}', name: 'app_vote')]
    public function index(ManagerRegistry $doctrine, Request $request, EntityManagerInterface $entityManager, int $id, int $ID): Response
    {
        $user = $this->getUser();

        if ($user != null){
            $post = $doctrine->getRepository(Posts::class)->find($id);
            $votes = $post->getVotes();
            $comment = $doctrine->getRepository(Comments::class)->find($ID);

            $foundUser = 0;
            foreach ($votes as $vote){
                if($vote->getUserRelation() == $user){
                    $foundUser = $user->getId();
                }
            }

            if($foundUser == 0){
                $newVote = new Vote;
                $newVote->setUserRelation($user);
                $newVote->setPostRelation($post);
                $newVote->setCommentRelation($comment);

                $entityManager->persist($newVote);

                $comment->setReputation($comment->getReputation()+1);
                $entityManager->persist($comment);

                $entityManager->flush();
            }
            return $this->redirectToRoute('app_post', ['id' => $id]);
        }
        

        return $this->redirectToRoute('app_login');
    }
}
