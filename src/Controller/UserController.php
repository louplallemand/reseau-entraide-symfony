<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UpdateUserType;
use App\Form\UpdatePasswordType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;



class UserController extends AbstractController
{
    #[Route('/profil', name: 'app_user')]
    public function index(): Response
    {
        return $this->render('user/index.html.twig', [
            
        ]);
    }

    #[Route('/updateuser', name: 'update_user')]
    public function update(Request $request, EntityManagerInterface $entityManager): Response
    {

        $user = $this->getUser();

        $form = $this->createForm(UpdateUserType::class, $user);
        $form->handleRequest($request);
        

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('app_user');
        }

        return $this->render('user/update.html.twig', [
            'UpdateForm' => $form->createView(),
        ]);
    }

    #[Route('/updatepassword', name: 'update_password')]
    public function updatepassword(Request $request,UserPasswordHasherInterface $userOldPasswordHasher,UserPasswordHasherInterface $userNewPasswordHasher, EntityManagerInterface $entityManager): Response
    {

        $user = $this->getUser();

        $form = $this->createForm(UpdatePasswordType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $userOldPasswordHasher->hashPassword(
                $user,
                $form->get('oldplainPassword')->getData()
            );
            //if ($userOldPasswordHasher === $user->getPassword()) {
            //pas fini pour le moment a reprendre plus tard:
            //le system pour check avec lancien mot de passe en mod double authentification
                $userNewPasswordHasher->hashPassword(
                    $user,
                    $form->get('newplainPassword')->getData()
                );

                $user->setPassword($userNewPasswordHasher->hashPassword(
                    $user,
                    $form->get('newplainPassword')->getData()
                ));
                $entityManager->persist($user);
                $entityManager->flush();

                return $this->render('user/UpdatePassword.html.twig', [
                    'UpdatePassword' => $form->createView(),
                    'message' => 'Password Changed successfully',
                ]);
            //}

            return $this->render('user/UpdatePassword.html.twig', [
                'UpdatePassword' => $form->createView(),
                'message' => 'Password verification failed',
            ]);
        }

        return $this->render('user/UpdatePassword.html.twig', [
            'UpdatePassword' => $form->createView(),
            'message' => '',
        ]);
    }
}
