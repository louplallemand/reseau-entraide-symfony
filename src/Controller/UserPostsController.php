<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Posts;
use App\Entity\Comments;
use App\Entity\User;
use App\Form\PostFormType;
use App\Form\CommentFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use App\Form\RegisterNewProjectType;

class UserPostsController extends AbstractController
{
    #[Route('/myposts', name: 'app_myposts')]
    public function index(ManagerRegistry $doctrine, Request $request, EntityManagerInterface $entityManager): Response
    {
        $user = $this->getUser();
        $myposts = $doctrine->getRepository(User::class)->find($user)->getUserPosts();

        return $this->render('user_posts/index.html.twig', [
            'myposts' => $myposts,
        ]);
    }

    #[Route('/deletepost/{id}', name: 'app_deletepost')]
    public function deletepost(ManagerRegistry $doctrine, Request $request, EntityManagerInterface $entityManager, int $id)
    {
        $user = $this->getUser();
        $post = $doctrine->getRepository(Posts::class)->find($id);
        $commentsofpost = $post->getPostComments();
        foreach($commentsofpost as $comment){
            $entityManager->remove($comment);
        }
        $entityManager->remove($post);
        $entityManager->flush();

        return $this->redirectToRoute('app_myposts');
    }
}
