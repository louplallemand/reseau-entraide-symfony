<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Posts;
use App\Entity\Comments;
use App\Entity\User;
use App\Form\PostFormType;
use App\Form\CommentFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use App\Form\RegisterNewProjectType;

class PostsController extends AbstractController
{
    #[Route('/', name: 'app_posts')]
    public function question(ManagerRegistry $doctrine, Request $request, EntityManagerInterface $entityManager): Response
    { 
        $questions = $doctrine->getRepository(Posts::class)->findAll();
        $questionsfiltered = [];
        foreach($questions as $question){
            if ($question->getState() !== 'disabled'){
                array_push($questionsfiltered, $question);
            }
        }
        
        $reversequestions = array_reverse($questionsfiltered);

        $post = new Posts;
        $user = $this->getUser();

        $post->setState('open');
        $form = $this->createForm(PostFormType::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($user != null){
                $post->setUserRel($user);
                $entityManager->persist($post);
                $entityManager->flush();

                return $this->redirectToRoute('app_posts');
            }

            return $this->redirectToRoute('app_login');
        }

        return $this->render('posts/index.html.twig', [
            'controller_name' => 'PostsController',
            'questionData' => $reversequestions,
            'PostForm' => $form->createView(),
        ]);
    }


    #[Route('/post/{id}', name: 'app_post')]
    public function post(ManagerRegistry $doctrine, Request $request, EntityManagerInterface $entityManager, int $id): Response
    { 
        $post = $doctrine->getRepository(Posts::class)->find($id);
        $comments = $post->getPostComments();

        $user = $this->getUser();
        $comment = new Comments;

        $form = $this->createForm(CommentFormType::class, $comment);
        $form->handleRequest($request);
        if ($post->getState() == 'open'){
            if ($form->isSubmitted() && $form->isValid()) {
                if ($user != null){
                    $comment->setRelUser($user);
                    $comment->setRelPosts($post);
                    $comment->setReputation(0);
                    $entityManager->persist($comment);
                    $entityManager->flush();
                    return $this->redirectToRoute('app_post', ['id' => $id]);
                }
                return $this->redirectToRoute('app_login');
            }
        }
        
        if ($post->getState() == 'open'){
            return $this->render('posts/post.html.twig', [
            'controller_name' => 'Post',
            'questionData' => $post,
            'CommentForm' => $form->createView(),
            'Comments' => $comments,
            ]);
        }else{
            return $this->render('posts/post.html.twig', [
                'controller_name' => 'Post',
                'questionData' => $post,
                'CommentForm' => false,
                'Comments' => $comments,
            ]);
        }
    }
}
